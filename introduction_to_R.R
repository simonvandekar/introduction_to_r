#### INTRODUCTION TO R ####
#Download and install R
#Download and install Rstudio

#Vectors, matrices, arrays, lists
vec = c(1, 3, 5, 7, 9)
vec1 = 1:9
dim(vec)
length(vec1)
dim(as.matrix(vec))
mat = matrix(1:9, nrow=3)
arr = array(1:12, dim = c(2,2,3))

listofthings = list(arr, mat, vec, 'other stuff')

# Matrix multiplication
mat %*% 1:3
# array outer product
mat %o% 1:3
# Not matrix multiplication!!
mat * 1:3
# still works, but with warning!
mat * 1:2

# load iris data
data(iris3) # not a data frame

# function to tidy iris data
tidydata = function(x, arr){
  out=as.data.frame(arr[,,x])
  out$Species = dimnames(arr)[[3]][x]
  out
}


df = lapply(1:3, tidydata, arr=iris3 )
df = do.call(rbind, df)
colnames(df) = gsub(' ', '.', colnames(df))
df$Species = as.factor(df$Species)

# Basic statistical tools
plot(df)

model = lm(Sepal.L. ~ Species, data=df)
summary(model)

# plotting
cex=1.5
par(mgp=c(1.7,.7,0), lwd=1.5, lend=2, cex.lab=0.8*cex, cex.axis=0.8*cex, cex.main=1*cex, mfrow=c(1,1), mar=c(2.8,2.8,1.8,.2))
plot(df$Petal.L., df$Sepal.L., col=c('#1b9e77', '#d95f02', '#7570b3')[df$Species], bty='l', ylab='Sepal Length', xlab='Petal Length', main='Iris Species' )
legend('topleft', bty='n', legend=levels(df$Species), col =c('#1b9e77', '#d95f02', '#7570b3'), pch = 1 )

# Getting packages
install.packages('mgcv')
install.packages('e1071')

# loading packages
library(mgcv)

# Getting help
?gam

data(AirPassengers)
ap = AirPassengers
ap = data.frame(passengers=c(ap), year=c(floor(time(ap))), month = c(cycle(ap)))

gmodel = gam(passengers ~ s(year) + s(month, bs = 'cs'), method='REML', data=ap)
gmodelred = gam(passengers ~ year + s(month, bs = 'cs'), method='REML', data=ap)

# create data for plotting
plotdata = data.frame(year=mean(ap$year), month=seq(1,12, length.out=100))
plotdata[, c('fit', 'se.fit')] = as.data.frame(predict(gmodel, plotdata, se.fit = TRUE))

# create plot is base R
cex=1.5
par(mgp=c(1.7,.7,0), lwd=1.5, lend=2, cex.lab=0.8*cex, cex.axis=0.8*cex, cex.main=1*cex, mfrow=c(1,1), mar=c(2.8,2.8,1.8,.2))
plot(ap$month, ap$passengers, main='Monthly Travel', ylab='Passengers', xlab='Month', ylim=c(0,700), bty='l')
lines(plotdata$month, plotdata$fit)
lines(plotdata$month, plotdata$fit + plotdata$se.fit, lty=2)
lines(plotdata$month, plotdata$fit - plotdata$se.fit, lty=2)

dev.off()



# tidyverse
# Free book by Hadley Wikham & Garrett Grolemund: http://r4ds.had.co.nz/
install.packages('tidyverse')
library(tidyverse)

# Same plot using ggplot
ggplot(data=ap, aes(month, passengers)) +
  geom_point() +
  geom_line(aes(month, fit), data = plotdata) +
  geom_line(aes(month, fit+se.fit), data = plotdata, linetype='dashed') +
  geom_line(aes(month, fit-se.fit), data = plotdata, linetype='dashed') +
  ggtitle('Monthly Travel')

# irises using ggplot
qplot(Sepal.Length, Petal.Length, data = iris, color = Species)




